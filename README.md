# Gitlab Ci Runner

## Getting started

Install Gitlab Runner
```
docker run -d --name gitlab-runner-medium --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:alpine
```

Register the runner (replace 'glrt-copytoken')
```
docker run --rm -t -i -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner:alpine register --non-interactive --url "https://gitlab.com/" --registration-token "glrt-copytoken" --executor "docker" --docker-image docker:stable --description " docker runner medium project" --docker-privileged --docker-tlsverify --docker-volumes "/certs/client"
```
